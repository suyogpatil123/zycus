#!/bin/bash
ACC_NAME='suyog' # assumption
SERVER_LIST=$1
HOST_FILE='/tmp/hosts'

if [[ $SERVER_LIST ]] ; then
  echo "$SERVER_LIST" | sed "s/,/\n/g" > "$HOST_FILE"
else
  echo "Please provide hostname parameter eg. $ sh parallel_ssh.sh HOST1,HOST2,HOST3"
  exit
fi

while true
do
  echo "Input command"
  read -r CMD
  if [[ $CMD ]] ; then
    pssh -h "$HOST_FILE" -l "$ACC_NAME" -i "$CMD"
  else
    echo "Please input non-empty command"
  fi
done
